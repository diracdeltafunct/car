import zmq
import time
import sys

#from car.server.handler import Handler

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:%s" % port)

import pygame
import pygame.camera

pygame.camera.init()
cams = pygame.camera.list_cameras() #Camera detected or not
print(cams)
cam = pygame.camera.Camera(cams[0],(640*0.5,480*0.5))
cam.start()

#andler = Handler()

while True:
    #  Wait for next request from client
    message = socket.recv()
    #status = handler.recieve(message)
    img = cam.get_image()

    #socket.send_string('%s' % status,  zmq.NOBLOCK)
    socket.send(pygame.image.tostring(img, 'RGB'))
    pygame.image.save(img, 'test.jpg')

