

from PyQt4 import QtGui, QtCore

class Main(QtGui.QWidget):
    def __init__(self):

        super().__init__()

        self.abort = AbortWindow(self)

        _layout = QtGui.QVBoxLayout()
        self.setLayout(_layout)

        self.btn = QtGui.QPushButton('sadfas')
        self.btn.pressed.connect(self.blah)
        _layout.addWidget(self.btn)
        self.abort.show()
    def blah(self):
        print('hi')

class AbortWindow(QtGui.QMainWindow):
    time = 0
    base = '<font color=white size=10>Waiting for Experiment to exit: %s</font>'

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        #self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setWindowModality(QtCore.Qt.WindowModal)
        _layout = QtGui.QVBoxLayout()
        self.setAutoFillBackground(True)
        central = QtGui.QWidget()
        self.setCentralWidget(central)
        self.setStyleSheet("background-color: black")
        central.setLayout(_layout)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.increment)
        self.timer.start(1000)

        self.label = QtGui.QLabel((self.base % self.time))

        _layout.addWidget(self.label)


    def increment(self):
        self.time +=1
        self.label.setText((self.base % self.time))
        if self.time > 5:
            self.hide()


if __name__ == '__main__':
    x = QtGui.QApplication([])
    win = Main()
    win.show()
    x.exec_()