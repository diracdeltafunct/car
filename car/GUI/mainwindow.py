from collections import OrderedDict
from PyQt4 import QtGui, QtCore
import PyQt4
from PIL import ImageQt, Image

from car.concurrancy import ControllerWorker
from car.concurrancy import ImageWorker
#from car.hardware.image_client import Image_Client

class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        super().__init__()
        self.setWindowTitle('Car Controller')

        central_widget = QtGui.QWidget()
        self.setCentralWidget(central_widget)


        self.buttons = OrderedDict([('Left Stick X', 0), ('Left Stick Y', 1), ('Right Stick Y', 3),
                                    ('Right Stick X',4),
                                    ('A', 5),
                                    ('B', 6),
                                    ('X', 7),
                                    ('Y', 8),
                                    ('Left Bumper', 9 ),
                                    ('Right Bumper', 10),
                                    ('Select', 11),
                                    ('Start', 12),
                                    ('Left Press', 13),
                                    ('Right Press', 14),
                                    ('Trigger',2),
                                    ])

        self.labels = OrderedDict()
        for b in self.buttons:
            self.labels[b] = InputValue(b)

        self.image = ImageStream(self)

        _layout = QtGui.QHBoxLayout()
        central_widget.setLayout(_layout)
        _button_layout = QtGui.QVBoxLayout()
        _layout.addLayout(_button_layout)

        [_button_layout.addWidget(w) for w in self.labels.values()]
        _layout.addWidget(self.image)
        self.start = QtGui.QPushButton('Start')
        self.start.pressed.connect(self.start_controller)

        _layout.addWidget(self.start)



    def start_controller(self):
        self.image.setup_worker()
        self.c = Controller(self)




class InputValue(QtGui.QLabel):
    _value = 0

    def __init__(self, label):
        self.label = label
        super().__init__()
        self.value = 0

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, val):
        self._value = val
        self.setText('%s: %.4f' % (self.label, val))


class Controller(QtGui.QWidget):

    def __init__(self, window):
        super().__init__()
        self.worker_thread = QtCore.QThread()

        self.worker = ControllerWorker(window)

        self.worker.moveToThread(self.worker_thread)
        self.worker.emit_value.connect(self.get_value)

        self.start()

    def start(self):
        if not self.worker_thread.isRunning():

            self.worker_thread.start()
            QtCore.QMetaObject.invokeMethod(self.worker, 'run', QtCore.Qt.QueuedConnection,

                                    )
    def get_value(self, value):
        pass


class ImageStream(QtGui.QLabel):

    def __init__(self, window):
        super().__init__()
        image = ImageQt.ImageQt(Image.new('RGB', (int(640*0.5), int(480*0.5))))
        pixmap = QtGui.QPixmap.fromImage(image)
        self.setPixmap(pixmap)
        self.timer = QtCore.QTimer()
        self.worker_thread = QtCore.QThread()
        #self.timer.timeout.connect(self.update)


    def setup_worker(self):
        self.worker = ImageWorker(self)

        self.worker.moveToThread(self.worker_thread)
        self.worker.image_update.connect(self.update)
        self.start()

    def update(self, img):
        pixmap = self.pil2pixmap(img)
        self.setPixmap(pixmap)

    def pil2pixmap(self,im):
        if im.mode == "RGB":
            pass
        elif im.mode == "L":
            im = im.convert("RGBA")
        data = im.convert("RGBA").tostring("raw", "RGBA")
        qim = QtGui.QImage(data, im.size[0], im.size[1], QtGui.QImage.Format_ARGB32)
        pixmap = QtGui.QPixmap.fromImage(qim)
        return pixmap



    def start(self):
        if not self.worker_thread.isRunning():

            self.worker_thread.start()
            QtCore.QMetaObject.invokeMethod(self.worker, 'run', QtCore.Qt.QueuedConnection,

                                    )



if __name__ == '__main__':
    app = QtGui.QApplication([])
    window = MainWindow()
    window.show()
    app.exec_()