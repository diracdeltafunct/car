import zmq
import sys
import simplejson as json
from PIL import Image
import pygame
port = "5557"

context = zmq.Context()
print("Connecting to server...")
socket = context.socket(zmq.REQ)
socket.connect ("tcp://localhost:%s" % port)


test = {'Blarg': 1}

#  Do 10 requests, waiting each time for a response
for request in range(1,10):
    print("Sending request ", request, "...")
    socket.send_string(json.dumps(test))
    #  Get the reply.
    message = socket.recv()
    print(type(message))
    img = Image.frombytes('RGBA', (640*0.5,480*0.5), message)

    print(img)
    #pygame.image.save(message, 'img.jpg')
    #print("Received reply ", request, "[", message, "]")