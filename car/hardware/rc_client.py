import zmq
import sys
import simplejson as json




class RC_Client(object):

    def __init__(self, ip='10.0.0.31', port='5556'):
        self.ip = ip
        self.port = port
        context = zmq.Context()
        #print("Connecting to server...")
        self.socket = context.socket(zmq.REQ)
        self.socket.connect("tcp://%s:%s" % (self.ip, self.port))


    def send(self, command):
        self.socket.send_string(json.dumps(command))
        return self.socket.recv_string()


if __name__== "__main__":
    c = RC_Client(ip='localhost')
    for i in range(10):
        response = c.send({'test': 1})
