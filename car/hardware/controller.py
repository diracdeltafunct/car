
"""
Gamepad Module
Daniel J. Gonzalez
dgonz@mit.edu

Based off code from: http://robots.dacloughb.com/project-1/logitech-game-pad/
"""

"""
Returns a vector of the following form:
[LThumbstickX, LThumbstickY, Unknown Coupled Axis???,
RThumbstickX, RThumbstickY,
Button 1/X, Button 2/A, Button 3/B, Button 4/Y,
Left Bumper, Right Bumper, Left Trigger, Right Triller,
Select, Start, Left Thumb Press, Right Thumb Press]

Note:
No D-Pad.
Triggers are switches, not variable.
Your controller may be different
"""

from PyQt4 import QtGui, QtCore
import pygame


class Controller(QtCore.QObject):
    values = QtCore.pyqtSignal(list)

    def __init__(self):
        super().__init__()
        self.controller = self.connect()
        self.is_alive = True

    def connect(self):
        pygame.init()
        j = pygame.joystick.Joystick(0)
        j.init()
        print('Initialized Joystick : %s' % j.get_name())
        return j

    def get(self, j):
        out = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        it = 0 #iterator
        pygame.event.pump()

        #Read input from the two joysticks
        for i in range(0, j.get_numaxes()):
            out[it] = j.get_axis(i)
            it+=1
        #Read input from buttons
        for i in range(0, j.get_numbuttons()):
            out[it] = j.get_button(i)
            it+=1
        return out


    def stream(self):
        while self.is_alive:
            self.values.emit(self.get(self.controller))




if __name__ == '__main__':
    controller = connect()
    test(controller)