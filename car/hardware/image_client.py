import zmq
from PIL import Image
from PyQt4 import QtCore
import pygame


class Image_Client(QtCore.QObject):
    size = (int(640*0.5), int(480*0.5))
    emit_value = QtCore.pyqtSignal(object)
    def __init__(self, ip='10.0.0.31', port='5557'):
        super().__init__()
        self.ip = '10.0.0.31'
        self.port = port
        print('got here')
        print(self.ip)
        context = zmq.Context()
        self.is_alive = True
        self.socket = context.socket(zmq.REQ)
        self.socket.connect("tcp://%s:%s" % (self.ip, self.port))

    def get(self):

        self.socket.send_string('image')
        buff = self.socket.recv()
        #print('rec')
        #buff = pygame.image.tostring(buff, 'RGBA')
        img = Image.frombytes('RGB', self.size, buff)
        return img

    def stream(self):
        print('got here')
        while True:
            #print('and here')

            self.emit_value.emit(self.get())




if __name__== "__main__":
    c = Image_Client(ip='192.168.0.100')
    x = c.get()
    from matplotlib import pyplot as plt
    #x = Image.frombytes('RGB', (640, 480), x)
    plt.imshow(x)
    plt.show()


