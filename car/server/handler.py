
import simplejson as json
from collections import OrderedDict

import RPi.GPIO as gpio

from car.hardware.drivers.Adafruit_PWM_Servo_Driver.Adafruit_PWM_Servo_Driver import PWM

pwm = PWM(0x40)
pwm.setPWMFreq(60)

class Handler(object):

    def __init__(self):
        self.car = Car()

    def recieve(self, command):
        valid, command_dict = self.validate(command)

        if valid:
            for key, value in command_dict.items():
                self.car.set_value(key, value)
            return 1
        else:
            return 0

    def validate(self, command):

        c = json.loads(command)
        if type(c) is dict:
            return True, c
        else:
            return False, c


class Car(object):
    _map = OrderedDict([('Left Stick X', {'action': 'drive', 'pin':5, 'value':0} ),
                        ('Left Stick Y', {'action': None, 'pin':5, 'value':0}),
                        ('Right Stick X', {'action': 'pan_f', 'pin':5, 'value':0}),
                        ('Right Stick Y',{'action': 'tilt_f', 'pin':5, 'value':0}),
                        ('A', {'action': 'light', 'pin':3, 'value':0}),
                        ('B', {'action': None, 'pin':5, 'value':0}),
                        ('X', {'action': None, 'pin':5, 'value':0}),
                        ('Y', {'action': None, 'pin':5, 'value':0}),
                        ('Left Bumper', {'action': None, 'pin':5, 'value':0}),
                        ('Right Bumper', {'action': None, 'pin':5, 'value':0}),
                        ('Select', {'action': None, 'pin':5, 'value':0}),
                        ('Start', {'action': None, 'pin':5, 'value':0}),
                        ('Left Press', {'action': None, 'pin':5, 'value':0}),
                        ('Right Press', {'action': None, 'pin':5, 'value':0}),
                        ('Trigger',{'action': 'drive', 'pin':5, 'value':0}),
                                    ])

    def __init__(self):
        gpio.setmode(gpio.BOARD)
        #gpio.setup(3, gpio.OUT)
        self.left_wheel = WheelServo(2)
        self.right_wheel = WheelServo(3)
        self.pan = CameraServo(1)
        self.tilt = CameraServo(0)

    def set_value(self, target, value):
        self._map[target]['value'] = value

        action = self._map[target]['action']
        print(target, value, action)
        if action is not None:
            if hasattr(self, action):
                funct = getattr(self, action)
                print('trying to call')
                print(target, value, action)
                funct(target, value)

    def light(self, target, value):
        pass
        """
        if value == 1:
            v = gpio.HIGH
        else:
            v= gpio.LOW
        gpio.output(self._map[target]['pin'], v)
        """

    def drive(self, *args):

        steer = self._map['Left Stick X']['value']
        speed = -self._map['Trigger']['value']


        if steer > 0:

            right_speed = self.steer(speed, steer)
            left_speed = speed
        else:
            right_speed = speed
            left_speed = self.steer(speed, steer)


        self.left_wheel.position =  left_speed * 100
        self.right_wheel.position = -right_speed * 100

    def steer(self, speed, steer):
        return (abs(speed) - abs(steer)) * self.magnitude(speed)

    def magnitude(self, value):
        if value >= 0:
            return 1
        else:
            return -1

   def pan_f(self, value):

        pos = value * 90
        self.pan.position = pos

    def tilt_f(self, value):

        pos = value * 90
        self.tilt.position = pos


class Servo:
    min = 1000
    max = 2000
    _position = 0

    def __init__(self, channel):
        self.channel = channel
        self.position = self._position
        

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, val):
        pulse_len = self.calc_pulse(val)
        self._set_position(pulse_len)
        self._position = val

    def calc_pulse(self, val):
        raise NotImplementedError

    def _set_position(self, pulse_len):
        print(int(pulse_len), self.channel)
        pwm.setPWM(self.channel, 0, int(pulse_len))



class WheelServo(Servo):
    min = 150
    max = 600

    def calc_pulse(self, angle):
        min = 0
        max = 200

        if angle > 100:
            angle = 100
        elif angle < -100:
            angle = -100

        a = (angle + 100) / max
        pulse = (a * (self.max - self.min)) + self.min
        print('pulse', pulse)
        return pulse

class CameraServo(Servo):
    min = 150
    max = 600

    def calc_pulse(self, angle):
        min = 0
        max = 180

        if angle > 90:
            angle = 90
        elif angle < -90:
            angle = -90

        a = (angle + 90) / max
        pulse = (a * (self.max - self.min)) + self.min
        return pulse
        
        
