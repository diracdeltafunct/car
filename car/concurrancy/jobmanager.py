import sys
import logging
from concurrent.futures import ProcessPoolExecutor, wait, as_completed, ThreadPoolExecutor
from collections import deque
import json


log = logging.getLogger(__name__)


def _id(obj):
    long_hash = "%x" % id(obj)
    short_hash = long_hash[-5:]
    return short_hash


def _show_frame(frame):
    msg = "Submitted from %s(...) in %s:%s" % (frame.f_code.co_name,
                                               frame.f_code.co_filename,
                                               frame.f_lineno)
    return msg


class JobManager:
    """A JobManager is a singleton that manages a background
    ProcessPoolExecutor.  By using its API, we centralize the execution of all
    background jobs, which will include logging around those jobs. The
    ProcessPoolExecutor is allocated to take advantage of multiple cores on the
    running machine.

    Note that any work submitted to this JobManager via submit() or map() has
    the following restrictions:

    1. The function itself, and all its args and kwargs, must be
    pickle-friendly.  What is pickle-friendly is covered in the Python docs[1].

    2. To be visible to the logging system, the function must exist inside a
    proper named module. Note also that due to 1, lambda's can't be used
    (because they can't be pickle'ed).

    3. The cost of serializing/deserializing the function's args and kwargs
    should be less than the cost of executing the function on its required
    inputs. Otherwise, you will fight against the machinery.

    Note further that a list of "in-flight futures" is maintained in this
    object merely for convenience/debugging. The contents of the list of
    in-flight futures should not be relied upon because this list is not
    maintained in a thread-safe manner.  You have been warned!

    [1]: https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled
    """

    def __init__(self, num_cores=4):
        #self.pool = ProcessPoolExecutor(num_cores)
        self.pool = ThreadPoolExecutor(num_cores)
        self.futures = deque()

    def submit(self, fn, *args, **kwargs):
        """Submit fn (a function) to the pool of processes, applying args and
        kwargs to it.

        This method will return immediately with a Future object, which can be
        inspected via future.result(). If the Future is not complete, this call
        will block until it does. If it is complete, it will either return the
        value returned from the applied function, or it will raise an exception
        if any exception was raised by the submitted work.

        As a side effect, the future will be added to self.futures, which
        enables the convenience methods, self.wait_all() and self.stream_all().
        """

        frame = sys._getframe(1)
        ##print(frame)

        log.info("About to submit() background work")
        fn_info = (
            "%s(...)" % fn.__name__,
            ("num_args", len(args)),
            ("num_kwargs", len(kwargs)),
        )
        log.info(_show_frame(frame))
        log.info("Function apply info: %s" % json.dumps(list(fn_info)))
        future = self.pool.submit(fn, *args, **kwargs)
        log.info("Work submit()'ed; received Future %s" % _id(future))
        self.futures.append(future)
        return future

    def map(self, fn, *iterables):
        """Execute the fn (function) against the list of iterables.

        Return an iterator that applies function to every item of iterable,
        yielding the results.

        Useful to quickly execute a function in parallel and inspect its
        results, by simply calling list() on this function's returned iterator.

        If additional iterable arguments are passed, function must take that
        many arguments and is applied to the items from all iterables in
        parallel. With multiple iterables, the iterator stops when the shortest
        iterable is exhausted."""
        frame = sys._getframe(1)
        log.info("About to map() background work")
        fn_info = (
            "%s(...)" % fn.__name__,
            ("num_iterables", len(iterables)),
        )
        log.info(_show_frame(frame))
        log.info("Function apply info: %s" % json.dumps(list(fn_info)))
        iterable = self.pool.map(fn, *iterables)
        log.info("Work map()'ed; received Iterable %s" % _id(iterable))
        return iterable

    def wait_all(self):
        """Wait for all the currently in-flight futures to complete, and return
        their set.  This will only be the set of "done" futures; those not yet
        done will not be returned."""
        completed = wait(self.futures)
        self.futures.clear()
        return completed.done

    def stream_all(self):
        """Immediately returns an iterator over in-flight Future objects. The
        Future objects will be returned as they complete, so calling .result()
        will yield back values or exceptions."""
        completed = as_completed(list(self.futures))
        self.futures.clear()
        return completed

    def collect(self):
        """Yield all completed Future objects, without affecting in-flight
        ones. Prunes the list of in-flight Futures as a side effect."""
        to_prune = []
        for future in self.futures:
            if future.done():
                log.info("Future %s completed; will prune" % _id(future))
                to_prune.append(future)
                yield future
        for completed in to_prune:
            self.futures.remove(completed)

    def running(self):
        """Yield all in-flight Future objects that are not yet complete."""
        for future in self.futures:
            if not future.done():
                yield future
                
    def kill(self):
        #print('kill method')
        for future in self.running():
            future.cancel()
            #print('canceling worker')



# "global" singleton instance
_job_manager = None


def get_job_manager(reset=False):
    """Return singleton instance of the JobManager."""
    global _job_manager
    if _job_manager is None or reset:
        _job_manager = JobManager()
    return _job_manager


__all__ = ["JobManager", "get_job_manager"]

# tests have been moved to "tests/test_job_manager.py"

