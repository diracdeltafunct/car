from PyQt4 import QtCore
import logging

from car.hardware.controller import Controller
from car.hardware.rc_client import RC_Client
from car.hardware.image_client import Image_Client

class ControllerWorker(QtCore.QObject):
    """object that gets transfered to a Qthread that just starts a subprocess, emits whatever comes back
    from the subproccess so that another element can get the data"""

    emit_value = QtCore.pyqtSignal(float)
    def __init__(self, window):
        super().__init__()
        self.window = window
        self.latest_values = self._set_initial_values()
        self.is_alive = True
        self.controller = Controller()
        self.rc_client = RC_Client()

        self.controller.values.connect(self.distribute)

    def _set_initial_values(self):
        initial = {}
        for key in self.window.buttons.keys():
            initial[key] = 0
        return initial

    @QtCore.pyqtSlot()
    def run(self):
        self.controller.stream()

    @QtCore.pyqtSlot(list)
    def distribute(self, values):
        command = {}
        for key, val in self.window.buttons.items():
            self.window.labels[key].value = values[val]
            if self.latest_values[key] != values[val]:
                self.latest_values[key] = values[val]
                command[key] = values[val]
        if command:
            #print('c', command)#, self.latest_values)
            self.rc_client.send(command)


class ImageWorker(QtCore.QObject):
    """object that gets transfered to a Qthread that just starts a subprocess, emits whatever comes back
    from the subproccess so that another element can get the data"""
    image_update = QtCore.pyqtSignal(object)
    def __init__(self, window):
        super().__init__()
        self.window = window

        self.is_alive = True

        self.image_client = Image_Client(ip='192.168.0.100')
        self.image_client.emit_value.connect(self.distribute)


    @QtCore.pyqtSlot()
    def run(self):
        print('starting streaming')
        self.image_client.stream()

    @QtCore.pyqtSlot(object)
    def distribute(self, img):
        self.image_update.emit(img)