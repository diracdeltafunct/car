import zmq
import time
import sys

#from car.server.handler import Handler

port = "5557"

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:%s" % port)

import pygame
import pygame.camera
#from SimpleCV import Camera

pygame.camera.init()
cams = pygame.camera.list_cameras() #Camera detected or not
cam = pygame.camera.Camera(cams[0],(int(640*0.5), int(480*0.5)))
print(cams, cam)
cam.start()

#andler = Handler()

while True:
    #  Wait for next request from client
    message = socket.recv()
    #print(message)
    #status = handler.recieve(message)
    img = cam.get_image()
    socket.send(pygame.image.tostring(img, 'RGB'))
    #socket.send_pyobj(img)


