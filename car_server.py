import zmq
import time
import sys

from car.server.handler import Handler

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:%s" % port)

handler = Handler()

while True:
    #  Wait for next request from client
    message = socket.recv()
    status = handler.recieve(message)
    socket.send_string('%s' % status,  zmq.NOBLOCK)